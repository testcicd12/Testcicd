FROM python:3

WORKDIR /usr/src/app
COPY ./site2 /usr/src/app/
RUN pip install --no-cache-dir -r ./requirements.txt
EXPOSE 8000
ENTRYPOINT ["python", "manage.py"]
CMD ["runserver", "0.0.0.0:8000"]

